// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyAP2eaQQm4hN_KYa87vNMpYoFS-ZdAWCO0",
    authDomain: "class-moria-135b0.firebaseapp.com",
    databaseURL: "https://class-moria-135b0.firebaseio.com",
    projectId: "class-moria-135b0",
    storageBucket: "class-moria-135b0.appspot.com",
    messagingSenderId: "768267451076",
  }
};
