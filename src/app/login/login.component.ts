import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;

  constructor(private authService:AuthService, private router:Router) { }

  onSubmit(){
    this.authService.login(this.email,this.password);
    this.router.navigate(['/Books']);
  }

  ngOnInit() {
  }

}
