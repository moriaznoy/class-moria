import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {
  
  email:string;
  password:string;

  constructor(private authService:AuthService, private router:Router) { }

  onSubmit(){
    this.authService.signup(this.email,this.password);
    this.router.navigate(['/Books']);
  }

  ngOnInit() {
  }

}
