import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  bookCollection:AngularFirestoreCollection;

  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}] 


  // getBooks(){
  //       const booksObservable = new Observable(
  //         observer =>{
  //           setInterval (
  //             ()=> observer.next(this.books),2000
  //           )
  //         }
  //       )
  //       return booksObservable;
  // }


  getBooks(userId:string):Observable<any[]>{
    //return this.db.collection('books').valueChanges({idField:'id'});
    this.bookCollection = this.db.collection(`users/${userId}/books`);
        console.log('Books collection created');
        return this.bookCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }

  getbook(userId:string, id:string):Observable<any>{
    return this.db.doc(`users/${userId}/books/${id}`).get();
  }  

  addBooks(userId:string,title:string, author:string){
  const book = {title:title, author:author};
  //this.db.collection('books').add(book);
  this.userCollection.doc(userId).collection('books').add(book);
  }

  updateBook(userId:string,id:string,title:string,author:string){
    this.db.doc(`users/${userId}/books/${id}`).update(
       {
         title:title,
         author:author
       }
     )
   }

  deleteBook(userId:string,id:string){
  this.db.doc(`users/${userId}/books/${id}`).delete();  
  }

  constructor(private db:AngularFirestore) { }

}
