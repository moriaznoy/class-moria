import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formtemp',
  templateUrl: './formtemp.component.html',
  styleUrls: ['./formtemp.component.css']
})
export class FormtempComponent implements OnInit {

  cities:object[] =  [{id:1, name:'Tel Aviv'},{id:2, name:'London'},{id:3, name:'Paris'}];
  temperature:number;
  city:string; 

  onSubmit(){
    this.router.navigate(['/temperature', this.temperature,this.city])
  }
  

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
