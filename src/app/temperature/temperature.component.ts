import { Temps } from './../interfaces/temps';
import { TempService } from './../temp.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.css']
})
export class TemperatureComponent implements OnInit {

  constructor(private route: ActivatedRoute, private tempService:TempService) { }
  likes:number = 0;
  city:string;
  temp:number;
  tempData$:Observable<Temps>;
  image:string;
  errorMessage:string;
  hasError:boolean;

  onSubmit() {
    this.likes++;
  }

  ngOnInit() {
    this.city= this.route.snapshot.params.city1;
    this.tempData$ = this.tempService.searchWeatherData(this.city);
    this.tempData$.subscribe(
      data => {
        console.log(data);
        this.temp = data.temperature;
        this.image = data.image;
      },
      error=> {
        this.hasError = true;
        this.errorMessage = error.message;
        console.log('in the component '+ error.message);
      }
    )
  }
}
