import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from './../books.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent implements OnInit {

  author:string;
  title:string;
  id:string;
  isedit:boolean = false;
  buttonText:string = "Add book" 
  userId:string;

  constructor(public authService:AuthService, private booksService:BooksService, private route:ActivatedRoute,
    private router: Router) { }

  onSubmit(){
    if(this.isedit){
      this.booksService.updateBook(this.userId,this.id,this.title,this.author);
      this.router.navigate(['/Books'])
    }else{
      this.booksService.addBooks(this.userId,this.title,this.author);
      this.router.navigate(['/Books'])
    }
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.user.subscribe(
      user =>{
        this.userId = user.uid;
        if(this.id){
          this.isedit =true;
          this.buttonText ="Update";
          this.booksService.getbook(this.userId,this.id).subscribe(
            book=>{
              this.author = book.data().author;
              this.title = book.data().title;
              console.log(this.author);
            }
          )
        }
      }
    )
    //console.log(this.id);

  }
}
