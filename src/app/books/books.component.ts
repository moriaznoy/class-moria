import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  books$:Observable<any>;
  panelOpenState = false;
  books: any;
  userId:string;

  constructor(private booksService:BooksService,public authService:AuthService) { }

  delete(bookid:string){
    this.booksService.deleteBook(this.userId,bookid)


  }

    ngOnInit() {
       this.authService.user.subscribe(
         user => {
           this.userId = user.uid;
       this.books$ = this.booksService.getBooks(this.userId);
       }
    )
  }
} 

