import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { TempsRaw } from './interfaces/temps-raw';
import { Temps } from './interfaces/temps';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TempService {

  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "c2addb75777e24415bc0705646e8a194";
  private IMP = "&units=metric";


  searchWeatherData(cityName:string):Observable<Temps>{
    return this.http.get<TempsRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
    .pipe(map(data => this.transformWeatherData(data)),
    catchError(this.handleError))
  }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error)
  }
  
  private transformWeatherData(data:TempsRaw):Temps{
    return {
      name:data.name,
      country:data.sys.country,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat: data.coord.lat,
      lon:data.coord.lon,
    }
  }
  
  constructor(private http:HttpClient) { }
}
